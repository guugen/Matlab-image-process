%线性变化法: 
%这是针对灰度图像的变换，从数据的角度看待灰度图既是一个二维数组。
%从数学的函数角度看，即将一个数字域映射到另一个数字域，且是线性映射。
%注意两个域的范围，图像显示是要求灰度级范围的，如果超出这个范围可就大事不妙了。
%如何不妙呢？不妨试一试吧。


%brief: 将图片的灰度线性的映射到你想要的范围[out_low out_high]
%parameter: 
%   out_low:期望范围的最低值
%   out_high:期望范围的最高值
%   image:想要变换的图像,要求一定要是一个uint8的数据格式
%return: 
%   reslut:输出一个uint8数据类型的图像（也就是一个二维数组）
%   parameters:线性变换的系数k（斜率）和c（截距）
function [reslut, parameters] = linear_transformation(out_low, out_high, image)
    image = double(image);
    min_pixel = min(min(image));
    max_pixel = max(max(image));
    k = (out_high - out_low)/(max_pixel - min_pixel);
    c = out_low - k*min_pixel;
    reslut = k*image+c;

    reslut = uint8(reslut);
    parameters = [k,c];
end
