



function result = iterative_thresholding(grayImage, K)
    % 初始化阈值
    threshold = graythresh(grayImage);
    
    % 迭代次数
    maxIterations = 30;
    
    % 迭代计算阈值
    for i = 1:maxIterations
        % 分割图像
        binaryImage = imbinarize(grayImage, threshold);
        
        % 计算前景和背景的平均灰度值
        foregroundMean = mean(mean(grayImage.*uint8(binaryImage)));
        backgroundMean = mean(mean(grayImage.*uint8((~binaryImage))));
        
        % 更新阈值为两个平均值的平均
        newThreshold = (foregroundMean + backgroundMean) / 2;
        
        % 判断阈值是否收敛
        if abs(newThreshold - threshold) < K
            break;
        end
        
        % 更新阈值
        threshold = newThreshold;
    end

    result = threshold;
end