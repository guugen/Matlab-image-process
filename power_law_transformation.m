%幂律变换也被称为幂次变换或幂次拉伸，是一种数字图像处理中常用的灰度变换方法。
%它通过对图像的像素值进行非线性的幂函数变换，来改变图像的对比度和亮度分布。
%较小的指数（γ<1）可以压缩亮度范围，增强暗部细节；
%较大的指数（γ>1）可以扩展亮度范围，增强亮部细节。


%brief:
%   对灰度图像进行幂律变换，如果不指定c,c将自己计算得出。
%   如果你要指定，小心变换后的灰度范围哦。
%parameters:
%   image：想要变换的图像，要求uint8的数据类型
%   y：幂律变换的指数
%   c：幂律变换的系数
%   L：图像的灰度级
%return：
%   reslut：变换后的图像，uint8数据类型
%   parameters：返回变换式中的c（缩放常数），y（幂律指数），L（灰度级）
function [reslut, parameters]= power_law_transformation(image, y, c, L)
    L = 256;
    image = double(image);
    c = (L-1)^(1-y);
    image = c*image.^y;

    reslut = uint8(image);
    parameters = [c,y,L];
end

