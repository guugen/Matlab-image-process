% 建立高斯滤波函数
function reslut = gaussian_high_pass(image, d0)
    image_fft = fft2(image);
    image_fft = fftshift(image_fft);
    [r, c] = size(image_fft);
    d00 = 2*d0;
    image_filter = zeros(r, c);
    for i = 1:r
        for j = 1:c
            d = sqrt((i-r/2)^2 + (j-c/2)^2);
            image_filter(i,j) = image_fft(i,j)*(1-exp(-d^2/d00^2));
        end
    end
    reslut =real(ifft2(ifftshift(image_filter)));
    
    reslut = linear_transformation(0,255,reslut);
end


