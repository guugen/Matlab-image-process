




function result = thresholding(image,K)
    [m, n] = size(image);

    for i = 1:m
        for j = 1:n
            if image(i,j) > K
                image(i,j) = 255;
            else
                image(i,j) = 0;
            end
        end
    end

    result = image;
end

















