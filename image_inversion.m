%图像反转，从图像的角度看，用于将图像的亮度值进行颠倒，即将亮的像素变暗，暗的像素变亮。
%这种操作可以通过线性变换法实现，想想看它的截距与斜率是多少吧，以及函数图像长什么样子。
%L - 1 -image; 这是书上的公式，对于8位灰度图，L取256。借此感受一下灰度级，灰度值之间的差异吧。
%有时我也会被绕晕，虽然他们有些纠缠不清，但具体实现时，关注数据本身，单纯的从数学的角度出发就好了。
%只要能实现单值映射，且映射值没错就OK。


%brief:反转图像的灰度值，将亮的变暗，暗的变亮。
%parameter:
%   L:图像的灰度级
%   image:想要变换的图像，要求一定要是一个uint8的数据格式
%return:
%   reslut:反转后的图像，uint8数据类型。
function reslut = image_inversion(L, image)
    reslut = L - 1 -image; %这是书上的公式，对于8位灰度图，L取256.
end






